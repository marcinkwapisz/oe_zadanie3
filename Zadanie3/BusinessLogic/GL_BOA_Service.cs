﻿using System;
using System.Collections.Generic;
using System.Linq;
using Zadanie3.Data;

namespace Zadanie3.BusinessLogic
{
    class GL_BOA_Service
    {
        private readonly Parameters parameters;
        private Butterfly[] butterflies;
        private double[] bestGlobalPosition;
        private double bestGlobalFitness;
        private int bestAmountOfIterations = int.MaxValue;
        private Results results = new Results();
        private double powerExponent;

        public GL_BOA_Service(Parameters parameters)
        {
            this.parameters = parameters;
            parameters.TournamentSelectionSize = parameters.NumberOfButterflies * parameters.TournamentSelectionSize / 100;
        }

        public void InitializePopulation(Parameters parameters)
        {
            butterflies = new Butterfly[parameters.NumberOfButterflies];
            bestGlobalPosition = new double[parameters.NumberOfDimensions];
            bestGlobalFitness = double.MaxValue;
            powerExponent = parameters.PowerExponentMin;
            for (int i = 0; i < butterflies.Length; i++)
            {
                double[] randomPosition = new double[parameters.NumberOfDimensions];
                for (int d = 0; d < parameters.NumberOfDimensions; d++)
                {
                    randomPosition[d] = HelperClass.GetRandomNumberFromRange(parameters.MinValue, parameters.MaxValue);
                }
                double fitness = ObjectiveFunction.GetFitnessToObjectiveFunction(parameters.Function, randomPosition);
                double fragrance = parameters.ModularModality * Math.Pow(fitness, powerExponent);
                butterflies[i] = new Butterfly(randomPosition, fitness, fragrance);
                if (butterflies[i].Fitness < bestGlobalFitness)
                {
                    bestGlobalFitness = butterflies[i].Fitness;
                    butterflies[i].Position.CopyTo(bestGlobalPosition, 0);
                }
            }
        }

        public void OptimizeWithGL_BOA()
        {
            List<double> averageFitnessInIteration = new List<double>();
            List<double> fitnessOfEachButterfly = new List<double>();
            List<int> iterationNumberForButterflies = new List<int>();
            int iteration;
            bool optimizationWasSuccessful = false;
            for (iteration = 1; iteration < parameters.NumberOfIterations; iteration++)
            {
                OptimizeWithBOA();
                for (int j = 0; j < butterflies.Length; j++)
                {
                    iterationNumberForButterflies.Add(iteration);
                    fitnessOfEachButterfly.Add(butterflies[j].Fitness);
                }
                double averageFitnessInPopulation = CalculateFitnessAverageValue();
                averageFitnessInIteration.Add(averageFitnessInPopulation);
                if (averageFitnessInPopulation < parameters.Accuracy)
                {
                    optimizationWasSuccessful = true;
                    break;
                }
                powerExponent = parameters.PowerExponentMin + ((iteration + 1) / (double)parameters.NumberOfIterations)
                    * (parameters.PowerExponentMax - parameters.PowerExponentMin);
            }
            if (iteration < bestAmountOfIterations)
            {
                results.FitnessFromBestOptimizationRun = averageFitnessInIteration;
                results.IterationNumberForParticles = iterationNumberForButterflies;
                results.FitnessOfEachParticle = fitnessOfEachButterfly;
                bestAmountOfIterations = iteration;
            }
            results.NumberOfIterations.Add(iteration);
            results.IfOptimizationWasSuccessful.Add(optimizationWasSuccessful);
            results.StandardDeviation.Add(HelperClass.CalculateStandardDeviation(butterflies.Select(b => b.Fitness)));
        }

        private void OptimizeWithBOA()
        {
            for (int i = 0; i < butterflies.Length; i++)
            {
                OffspringCrossover(butterflies[i]);
                OffspringMutation(butterflies[i]);
                OffspringSelection(butterflies[i]);
                UpdatePositionOfButterfly(butterflies[i]);
                CalculateFitnessOfButterfly(butterflies[i]);
                CalculateFragranceOfButterfly(butterflies[i]);
            }
        }

        private void OffspringCrossover(Butterfly butterfly)
        {
            for (int d = 0; d < parameters.NumberOfDimensions; d++)
            {
                int randomIndex = HelperClass.GetRandomIndexFromTable(butterflies);
                Butterfly randomButterfly = butterflies[randomIndex];
                double butterflyFitness = ObjectiveFunction.GetFitnessToObjectiveFunction(parameters.Function, butterfly.Position);
                double randomButterflyFitness = ObjectiveFunction.GetFitnessToObjectiveFunction(parameters.Function, randomButterfly.Position);
                if (butterflyFitness < randomButterflyFitness)
                {
                    double random = HelperClass.GetRandomNumberFromRange(0.0, 1.0);
                    butterfly.Offspring[d] = random * butterfly.Position[d] + (1 - random) * bestGlobalPosition[d];
                }
                else
                    butterfly.Offspring[d] = randomButterfly.Position[d];
            }
        }

        private void OffspringMutation(Butterfly butterfly)
        {
            for (int d = 0; d < parameters.NumberOfDimensions; d++)
            {
                double random = HelperClass.GetRandomNumberFromRange(0.0, 1.0);
                if (random < parameters.MutationProbability)
                    butterfly.Offspring[d] = HelperClass.GetRandomNumberFromRange(parameters.MinValue, parameters.MaxValue);
            }
        }

        private void OffspringSelection(Butterfly butterfly)
        {
            double offspringFitness = ObjectiveFunction.GetFitnessToObjectiveFunction(parameters.Function, butterfly.Offspring);
            double exemplarFitness = ObjectiveFunction.GetFitnessToObjectiveFunction(parameters.Function, butterfly.Exemplar);
            if (offspringFitness < exemplarFitness)
            {
                butterfly.Offspring.CopyTo(butterfly.Exemplar, 0);
                butterfly.StoppingGap = 0;
            }
            else
                butterfly.StoppingGap += 1;
            if (butterfly.StoppingGap == parameters.StoppingGap)
                TournamentSelection(butterfly);
        }

        private void TournamentSelection(Butterfly butterfly)
        {
            HashSet<Butterfly> partOfSwarm = new HashSet<Butterfly>();
            do
            {
                int randomIndex = HelperClass.GetRandomIndexFromTable(butterflies);
                partOfSwarm.Add(butterflies[randomIndex]);
            } while (partOfSwarm.Count != parameters.TournamentSelectionSize);
            butterfly.Exemplar = partOfSwarm.OrderBy(p => p.Fitness).First().Exemplar;
        }

        private void UpdatePositionOfButterfly(Butterfly butterfly)
        {
            double[] newPosition = new double[butterfly.Position.Length];
            double random = HelperClass.GetRandomNumberFromRange(0.0, 1.0);
            double randomSquared = random * random;
            if (random < parameters.SwitchProbability)
            {
                for (int d = 0; d < butterfly.Position.Length; d++)
                {
                    newPosition[d] = butterfly.Position[d] +
                        (randomSquared * butterfly.Exemplar[d] - butterfly.Position[d]) * butterfly.Fragrance;
                }
                newPosition.CopyTo(butterfly.Position, 0);
            }
            else
            {
                int index = HelperClass.GetRandomIndexFromTable(butterflies);
                for (int d = 0; d < butterfly.Position.Length; d++)
                {
                    newPosition[d] = butterfly.Position[d] + (randomSquared * butterflies[index].Exemplar[d]
                        - butterfly.Position[d]) * butterfly.Fragrance;
                }
                newPosition.CopyTo(butterfly.Position, 0);
            }
        }

        private void CalculateFitnessOfButterfly(Butterfly butterfly)
        {
            butterfly.Fitness = ObjectiveFunction.GetFitnessToObjectiveFunction(parameters.Function, butterfly.Position);
            if (butterfly.Fitness < bestGlobalFitness)
            {
                bestGlobalFitness = butterfly.Fitness;
                butterfly.Position.CopyTo(bestGlobalPosition, 0);
            }
        }

        private void CalculateFragranceOfButterfly(Butterfly butterfly)
        {
            butterfly.Fragrance = parameters.ModularModality * Math.Pow(butterfly.Fitness, powerExponent);
        }

        private double CalculateFitnessAverageValue()
        {
            double sum = 0.0;
            for (int i = 0; i < butterflies.Length; i++)
            {
                sum += butterflies[i].Fitness;
            }
            return sum / butterflies.Length;
        }

        public void SaveResultsOfOptimization()
        {
            results.SaveResultsToXml("GL_BOA");
        }
    }
}
