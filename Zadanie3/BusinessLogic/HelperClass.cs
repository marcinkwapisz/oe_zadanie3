﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Zadanie3.BusinessLogic
{
    class HelperClass
    {
        private static Random random = new Random();

        public static double GetRandomNumberFromRange(double min, double max)
        {
            return random.NextDouble() * (max - min) + min;
        }

        public static int GetRandomIndexFromTable<T>(T[] array)
        {
            int n = array.Length;
            return random.Next(0, n);
        }

        public static double CalculateStandardDeviation(IEnumerable<double> values)
        {
            double standardDeviation = 0;
            if (values.Any())
            {  
                double avg = values.Average();   
                double sum = values.Sum(d => Math.Pow(d - avg, 2));
                standardDeviation = Math.Sqrt((sum) / (values.Count() - 1));
            }      
            return standardDeviation;
        }
    }
}
