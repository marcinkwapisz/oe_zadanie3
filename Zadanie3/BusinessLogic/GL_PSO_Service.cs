﻿using System.Collections.Generic;
using System.Linq;
using Zadanie3.Data;

namespace Zadanie3.BusinessLogic
{
    class GL_PSO_Service
    {
        private readonly Parameters parameters;
        private Particle[] particles;
        private double[] bestGlobalPosition;
        private double bestGlobalFitness;
        private int bestAmountOfIterations = int.MaxValue;
        private Results results = new Results();

        public GL_PSO_Service(Parameters parameters)
        {
            this.parameters = parameters;
            parameters.TournamentSelectionSize = parameters.NumberOfParticles * parameters.TournamentSelectionSize / 100;
        }

        public void InitializeSwarm(Parameters parameters)
        {
            particles = new Particle[parameters.NumberOfParticles];
            bestGlobalPosition = new double[parameters.NumberOfDimensions];
            bestGlobalFitness = double.MaxValue;
            for (int i = 0; i < particles.Length; i++)
            {
                double[] randomPosition = new double[parameters.NumberOfDimensions];
                double[] randomVelocity = new double[parameters.NumberOfDimensions];
                for (int d = 0; d < parameters.NumberOfDimensions; d++)
                {
                    randomPosition[d] = HelperClass.GetRandomNumberFromRange(parameters.MinValue, parameters.MaxValue);
                    randomVelocity[d] = HelperClass.GetRandomNumberFromRange(parameters.MinValue * 0.1, parameters.MaxValue * 0.1);
                }
                double fitness = ObjectiveFunction.GetFitnessToObjectiveFunction(parameters.Function, randomPosition);

                particles[i] = new Particle(randomPosition, randomVelocity, fitness, randomPosition, fitness);
                if (particles[i].Fitness < bestGlobalFitness)
                {
                    bestGlobalFitness = particles[i].Fitness;
                    particles[i].Position.CopyTo(bestGlobalPosition, 0);
                }
            }
        }

        public void OptimizeWithGL_PSO()
        {
            List<double> averageFitnessInIteration = new List<double>();
            List<double> fitnessOfEachParticle = new List<double>();
            List<int> iterationNumberForParticles = new List<int>();
            int iteration;
            bool optimizationWasSuccessful = false;
            for (iteration = 1; iteration < parameters.NumberOfIterations; iteration++)
            {
                OptimizeWithPSO();
                for (int i = 0; i < particles.Length; i++)
                {
                    iterationNumberForParticles.Add(iteration);
                    fitnessOfEachParticle.Add(particles[i].Fitness);
                }
                double averageFitnessInSwarm = CalculateFitnessAverageValue();
                averageFitnessInIteration.Add(averageFitnessInSwarm);
                if (averageFitnessInSwarm < parameters.Accuracy)
                {
                    optimizationWasSuccessful = true;
                    break;
                }
            }
            if (iteration < bestAmountOfIterations)
            {
                results.FitnessFromBestOptimizationRun = averageFitnessInIteration;
                results.IterationNumberForParticles = iterationNumberForParticles;
                results.FitnessOfEachParticle = fitnessOfEachParticle;
                bestAmountOfIterations = iteration;
            }
            results.NumberOfIterations.Add(iteration);
            results.IfOptimizationWasSuccessful.Add(optimizationWasSuccessful);
            results.StandardDeviation.Add(HelperClass.CalculateStandardDeviation(particles.Select(b => b.Fitness)));
        }

        private void OptimizeWithPSO()
        {
            for (int i = 0; i < particles.Length; i++)
            {
                OffspringCrossover(particles[i]);
                OffspringMutation(particles[i]);
                OffspringSelection(particles[i]);
                UpdateVelocityOfParticle(particles[i]);
                UpdatePositionOfParticle(particles[i]);
                CalculateFitnessOfParticle(particles[i]);
            }
        }

        private void OffspringCrossover(Particle particle)
        {
            for (int d = 0; d < parameters.NumberOfDimensions; d++)
            {
                int randomIndex = HelperClass.GetRandomIndexFromTable(particles);
                Particle randomParticle = particles[randomIndex];
                double particleFitness = ObjectiveFunction.GetFitnessToObjectiveFunction(parameters.Function, particle.Position);
                double randomParticleFitness = ObjectiveFunction.GetFitnessToObjectiveFunction(parameters.Function, randomParticle.Position);
                if (particleFitness < randomParticleFitness)
                {
                    double random = HelperClass.GetRandomNumberFromRange(0.0, 1.0);
                    particle.Offspring[d] = random * particle.BestPosition[d] + (1 - random) * bestGlobalPosition[d];
                }
                else
                    particle.Offspring[d] = randomParticle.Position[d];
            }
        }

        private void OffspringMutation(Particle particle)
        {
            for (int d = 0; d < parameters.NumberOfDimensions; d++)
            {
                double random = HelperClass.GetRandomNumberFromRange(0.0, 1.0);
                if (random < parameters.MutationProbability)
                    particle.Offspring[d] = HelperClass.GetRandomNumberFromRange(parameters.MinValue, parameters.MaxValue);
            }
        }

        private void OffspringSelection(Particle particle)
        {
            double offspringFitness = ObjectiveFunction.GetFitnessToObjectiveFunction(parameters.Function, particle.Offspring);
            double exemplarFitness = ObjectiveFunction.GetFitnessToObjectiveFunction(parameters.Function, particle.Exemplar);
            if (offspringFitness < exemplarFitness)
            {
                particle.Offspring.CopyTo(particle.Exemplar, 0);
                particle.StoppingGap = 0;
            }
            else
                particle.StoppingGap += 1;
            if (particle.StoppingGap == parameters.StoppingGap)
                TournamentSelection(particle);
        }

        private void TournamentSelection(Particle particle)
        {
            HashSet<Particle> partOfSwarm = new HashSet<Particle>();
            do
            {
                int randomIndex = HelperClass.GetRandomIndexFromTable(particles);
                partOfSwarm.Add(particles[randomIndex]);
            } while (partOfSwarm.Count != parameters.TournamentSelectionSize);
            particle.Exemplar = partOfSwarm.OrderBy(p => p.Fitness).First().Exemplar;
        }

        private void UpdateVelocityOfParticle(Particle particle)
        {
            double[] newVelocity = new double[particle.Velocity.Length];
            for (int i = 0; i < particle.Velocity.Length; i++)
            {
                double random = HelperClass.GetRandomNumberFromRange(0.0, 1.0);
                newVelocity[i] = (parameters.Weight * particle.Velocity[i]) +
                (parameters.Coefficient * random * (particle.Exemplar[i] - particle.Position[i]));
            }
            newVelocity.CopyTo(particle.Velocity, 0);
        }

        private void UpdatePositionOfParticle(Particle particle)
        {
            double[] newPosition = new double[particle.Position.Length];
            for (int i = 0; i < particle.Position.Length; i++)
            {
                newPosition[i] = particle.Position[i] + particle.Velocity[i];
            }
            newPosition.CopyTo(particle.Position, 0);
        }

        private void CalculateFitnessOfParticle(Particle particle)
        {
            particle.Fitness = ObjectiveFunction.GetFitnessToObjectiveFunction(parameters.Function, particle.Position);
            if (particle.Fitness < particle.BestFitness)
            {
                particle.BestFitness = particle.Fitness;
                particle.Position.CopyTo(particle.BestPosition, 0);
                if (particle.BestFitness < bestGlobalFitness)
                {
                    bestGlobalFitness = particle.BestFitness;
                    particle.BestPosition.CopyTo(bestGlobalPosition, 0);
                }
            }
        }

        private double CalculateFitnessAverageValue()
        {
            double sum = 0.0;
            for (int i = 0; i < particles.Length; i++)
            {
                sum += particles[i].Fitness;
            }
            return sum / particles.Length;
        }

        public void SaveResultsOfOptimization()
        {
            results.SaveResultsToXml("GL_PSO");
        }
    }
}
