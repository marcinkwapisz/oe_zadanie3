﻿using static Zadanie3.BusinessLogic.ObjectiveFunction;

namespace Zadanie3
{
    public class Parameters
    {
        public int NumberOfRepetitions { get; set; }
        public FunctionTypeEnum Function { get; set; }
        public int NumberOfDimensions { get; set; }
        public double MinValue { get; set; }
        public double MaxValue { get; set; }
        public int NumberOfIterations { get; set; }
        public double Accuracy { get; set; }

        public int NumberOfParticles { get; set; }
        public double Weight { get; set; }
        public double Coefficient { get; set; }
        public double MutationProbability { get; set; }
        public int StoppingGap { get; set; }
        public int TournamentSelectionSize { get; set; }

        public int NumberOfButterflies { get; set; }
        public double ModularModality { get; set; }
        public double PowerExponentMin { get; set; }
        public double PowerExponentMax { get; set; }
        public double SwitchProbability { get; set; }

        public Parameters(int numberOfRepetitions, FunctionTypeEnum function, int numberOfDimensions,
            double minValue, double maxValue, int numberOfIterations, double accuracy, int numberOfParticles,
            double weight, double coefficient, double mutationProbability, int stoppingGap, int tournamentSelectionSize)
        {
            NumberOfRepetitions = numberOfRepetitions;
            Function = function;
            NumberOfDimensions = numberOfDimensions;
            MinValue = minValue;
            MaxValue = maxValue;
            NumberOfIterations = numberOfIterations;
            Accuracy = accuracy;
            NumberOfParticles = numberOfParticles;
            Weight = weight;
            Coefficient = coefficient;
            MutationProbability = mutationProbability;
            StoppingGap = stoppingGap;
            TournamentSelectionSize = tournamentSelectionSize;
        }

        public Parameters(int numberOfRepetitions, int numberOfDimensions, FunctionTypeEnum function,
            double minValue, double maxValue, int numberOfIterations, double accuracy, int numberOfButterflies,
            double modularModality, double powerExponentMin, double powerExponentMax, double switchProbability,
            double mutationProbability, int stoppingGap, int tournamentSelectionSize)
        {
            NumberOfRepetitions = numberOfRepetitions;
            Function = function;
            NumberOfDimensions = numberOfDimensions;
            MinValue = minValue;
            MaxValue = maxValue;
            NumberOfIterations = numberOfIterations;
            Accuracy = accuracy;
            NumberOfButterflies = numberOfButterflies;
            ModularModality = modularModality;
            PowerExponentMin = powerExponentMin;
            PowerExponentMax = powerExponentMax;
            SwitchProbability = switchProbability;
            MutationProbability = mutationProbability;
            StoppingGap = stoppingGap;
            TournamentSelectionSize = tournamentSelectionSize;
        }

        public Parameters()
        {
        }
    }
}
