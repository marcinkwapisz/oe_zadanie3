﻿namespace Zadanie3.Data
{
    class Butterfly
    {
        public double[] Position { get; set; }
        public double Fitness { get; set; }
        public double Fragrance { get; set; }
        public double[] Offspring { get; set; }
        public double[] Exemplar { get; set; }
        public int StoppingGap { get; set; }

        public Butterfly(double[] position, double fitness, double fragrance)
        {
            Position = new double[position.Length];
            position.CopyTo(Position, 0);
            Fitness = fitness;
            Fragrance = fragrance;
            Offspring = new double[Position.Length];
            Exemplar = new double[Position.Length];
            Position.CopyTo(Exemplar, 0);
            StoppingGap = 0;
        }
    }
}
