﻿using System;
using System.IO;
using System.Xml.Serialization;
using Zadanie3.BusinessLogic;

namespace Zadanie3
{
    class Program
    {
        static void Main(string[] args)
        {
            string parametersPath = AppDomain.CurrentDomain.BaseDirectory + "../../PSO_parameters.xml";
            //string parametersPath = AppDomain.CurrentDomain.BaseDirectory + "../../BOA_parameters.xml";

            Parameters retrievedParameters = new Parameters();
            using (FileStream fileStream = new FileStream(parametersPath, FileMode.Open))
            {
                XmlSerializer serializer = new XmlSerializer(retrievedParameters.GetType());
                retrievedParameters = serializer.Deserialize(fileStream) as Parameters;
            }

            GL_PSO_Service gL_PSO_Service = new GL_PSO_Service(retrievedParameters);
            for (int i = 0; i < retrievedParameters.NumberOfRepetitions; i++)
            {
                gL_PSO_Service.InitializeSwarm(retrievedParameters);
                gL_PSO_Service.OptimizeWithGL_PSO();
            }
            gL_PSO_Service.SaveResultsOfOptimization();

            //GL_BOA_Service gL_BOA_Service = new GL_BOA_Service(retrievedParameters);
            //for (int i = 0; i < retrievedParameters.NumberOfRepetitions; i++)
            //{
            //    gL_BOA_Service.InitializePopulation(retrievedParameters);
            //    gL_BOA_Service.OptimizeWithGL_BOA();
            //}
            //gL_BOA_Service.SaveResultsOfOptimization();
        }
    }
}
