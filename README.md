## GL-PSO, GL-BOA
This project was written using .NET Framework 4.6.1.

It consists of the self-written code of various bio-inspired, evolutionary calculation methods using a pattern:
* GL-PSO ([Genetic Learning Particle Swarm Optimization](https://ieeexplore.ieee.org/stamp/stamp.jsp?arnumber=7271066))
* GL-BOA (Genetic Learning Butterfly Optimization Algorithm)

The main goal was to find minimum values of functions of several variables (Rosenbrock, Griewank, Rastrigin) and comparison of the effectiveness and results of given algorithms.